    var l = window.location
    var ws = new WebSocket('ws://'+l.host + '/builderzws/testing')

    window.requestAnimFrame = (function(){
          return  window.requestAnimationFrame       ||
                  window.webkitRequestAnimationFrame ||
                  window.mozRequestAnimationFrame    ||
                  window.oRequestAnimationFrame      ||
                  window.msRequestAnimationFrame     ||
                  function(callback, element){
                    window.setTimeout(callback, 1000 / 60);
                  };
    })();

    var SCALE = 30;
    var NULL_CENTER = {x:null, y:null};

    function PulleyJoint(entity1, entity2, anchor1, anchor2) {
      this.entity1 = entity1;
      this.entity2 = entity2;
      this.anchor1 = anchor1;
      this.anchor2 = anchor2;
    }

    PulleyJoint.prototype.draw = function(ctx) {
      this.drawLine(ctx, this.entity1, this.anchor1);
      this.drawLine(ctx, this.entity2, this.anchor2);
    }

    PulleyJoint.prototype.drawLine = function(ctx, entity, anchor) {
      ctx.beginPath();
      ctx.strokeStyle = 'black';
      ctx.moveTo(entity.x * SCALE, entity.y * SCALE);
      ctx.lineTo(anchor.x * SCALE, anchor.y * SCALE);
      ctx.closePath();
      ctx.stroke();
    }

    function Entity(id, x, y, angle, center, color) {
      this.id = id;
      this.x = x;
      this.y = y;
      this.angle = angle || 0;
      this.center = center;
      this.color = color || "green";
      this.born = true;
      this.selected = false;
    }

    Entity.prototype.update = function(state) {
      this.x = state.x;
      this.y = state.y;
      this.center = state.c;
      this.angle = state.a;
    }

    Entity.prototype.draw = function(ctx) {
      ctx.fillStyle = 'black';
      ctx.beginPath();
      ctx.arc(this.x * SCALE, this.y * SCALE, 4, 0, Math.PI * 2, true);
      ctx.closePath();
      ctx.fill();

      ctx.fillStyle = 'gray';
      ctx.beginPath();
      ctx.arc(this.center.x * SCALE, this.center.y * SCALE, 2, 0, Math.PI * 2, true);
      ctx.closePath();
      ctx.fill();
    }

    Entity.build = function(def) {
      if (def.radius) {
        return new CircleEntity(def.id, def.x, def.y, def.angle, NULL_CENTER, def.color, def.radius);
      } else if (def.polys) {
        return new PolygonEntity(def.id, def.x, def.y, def.angle, NULL_CENTER, def.color, def.polys);
      } else {
        return new RectangleEntity(def.id, def.x, def.y, def.angle, NULL_CENTER, def.color, def.halfWidth, def.halfHeight);
      }
    }

    function CircleEntity(id, x, y, angle, center, color, radius) {
      color = color || 'aqua';
      Entity.call(this, id, x, y, angle, center, color);
      this.radius = radius;
    }

    CircleEntity.prototype = new Entity();
    CircleEntity.prototype.constructor = CircleEntity;

    CircleEntity.prototype.draw = function(ctx) {
      ctx.save();
      ctx.translate(this.x * SCALE, this.y * SCALE);
      ctx.rotate(this.angle);
      ctx.translate(-(this.x) * SCALE, -(this.y) * SCALE);

      ctx.fillStyle = this.color;
      ctx.strokeStyle = 'black';
      ctx.beginPath();
      ctx.arc(this.x * SCALE, this.y * SCALE, this.radius * SCALE, 0, Math.PI * 2, true);
      ctx.moveTo(this.x * SCALE, this.y * SCALE);
      ctx.lineTo((this.x) * SCALE, (this.y + this.radius) * SCALE);
      ctx.closePath();
      ctx.fill();
      ctx.stroke();

      ctx.restore();

      Entity.prototype.draw.call(this, ctx);
    }

    function RectangleEntity(id, x, y, angle, center, color, halfWidth, halfHeight) {
      Entity.call(this, id, x, y, angle, center, color);
      this.halfWidth = halfWidth;
      this.halfHeight = halfHeight;

    }
    RectangleEntity.prototype = new Entity();
    RectangleEntity.prototype.constructor = RectangleEntity;

    RectangleEntity.prototype.setHalfDimensions = function(halfW, halfH) {
      this.halfWidth = halfW;
      this.halfHeight = halfH;
    }

    RectangleEntity.prototype.draw = function(ctx) {
      ctx.save();
      ctx.translate(this.x * SCALE, this.y * SCALE);
      ctx.rotate(this.angle);
      ctx.translate(-(this.x) * SCALE, -(this.y) * SCALE);
      ctx.fillStyle = this.color;
      ctx.strokeStyle = 'green';
      ctx.lineWidth = 3;
      ctx.fillRect((this.x-this.halfWidth) * SCALE,
                   (this.y-this.halfHeight) * SCALE,
                   (this.halfWidth*2) * SCALE,
                   (this.halfHeight*2) * SCALE);
      if (this.selected) {
        ctx.strokeRect((this.x-this.halfWidth) * SCALE,
                     (this.y-this.halfHeight) * SCALE,
                     (this.halfWidth*2) * SCALE,
                     (this.halfHeight*2) * SCALE);
      }
      ctx.restore();

      Entity.prototype.draw.call(this, ctx);
    }

    function PolygonEntity(id, x, y, angle, center, color, polys) {
      Entity.call(this, id, x, y, angle, center, color);
      this.polys = polys;

    }
    PolygonEntity.prototype = new Entity();
    PolygonEntity.prototype.constructor = PolygonEntity;

    PolygonEntity.prototype.draw = function(ctx) {
      ctx.save();
      ctx.translate(this.x * SCALE, this.y * SCALE);
      ctx.rotate(this.angle);
      ctx.translate(-(this.x) * SCALE, -(this.y) * SCALE);
      ctx.fillStyle = this.color;

      for (var i = 0; i < this.polys.length; i++) {
        var points = this.polys[i];
        ctx.beginPath();
        ctx.moveTo((this.x + points[0].x) * SCALE, (this.y + points[0].y) * SCALE);
        for (var j = 1; j < points.length; j++) {
           ctx.lineTo((points[j].x + this.x) * SCALE, (points[j].y + this.y) * SCALE);
        }
        ctx.lineTo((this.x + points[0].x) * SCALE, (this.y + points[0].y) * SCALE);
        ctx.closePath();
        ctx.fill();
        ctx.stroke();
      }

      ctx.restore();

      Entity.prototype.draw.call(this, ctx);
    }

    function DrawToggle(onId, offId) {
      this.onId = onId;
      this.offId = offId;
      this.onRadio = document.getElementById(onId);
      this.offRadio = document.getElementById(offId);
    }
    DrawToggle.prototype.isDrawing = function() {
      return this.onRadio.checked ;
    }

    var world = {};
    var bodiesState = null;
    var box = null;
    var drawToggle = new DrawToggle("draw-on", "draw-off");
    var newRectangle = null;
    var addedRectangles = [];
    var removedRectangles = [];
    var copyButton = document.getElementById("copy-rect");
    var selectedId = null;

    ws.onmessage = function(e) {
            var jzn = JSON.parse(e.data)
            var newRectangle2 = new RectangleEntity(Date.now(), jzn.x, jzn.y, 0, NULL_CENTER, null, jzn.width, jzn.height);
            console.log("create rect{ x:" + newRectangle2.x+" y:"+newRectangle2.y+" } ")
            console.log("create rect{ width:" + newRectangle2.halfWidth+" height:"+newRectangle2.halfHeight+" } ")

            newRectangle2.born = true;
            world[newRectangle2.id] = newRectangle2;
            box.addBody(newRectangle2);
            addedRectangles.push(newRectangle2);
            box.update();

            console.log("DONE")
      }

    function update(animStart) {
      if (!drawToggle.isDrawing()) {
        if (isMouseDown) {
          box.mouseDownAt(mouseX, mouseY);
        } else if (box.isMouseDown()) {
          box.mouseUp();
        }
      } else {
        if (isMouseDown) {
          selectedId = box.getBodyIdAt(mouseX, mouseY);
        }
        if (isMouseDown && selectedId) {
          for (var k in world) {
            world[k].selected = false;
          }

          world[selectedId].selected = true;
          isMouseDown = false;
          copyButton.disabled = false;
        } else if (isMouseDown && newRectangle == null) {
          // start drawing
          newRectangle = new RectangleEntity(Date.now(), mouseX, mouseY, 0, NULL_CENTER, null, 0, 0);
          newRectangle.born = false;
          world[newRectangle.id] = newRectangle;
        } else if (isMouseDown && newRectangle) {
          // resize
          newRectangle.setHalfDimensions(Math.abs((mouseX - newRectangle.x) / 2), Math.abs((mouseY - newRectangle.y) / 2));
        } else if (!isMouseDown && newRectangle) {
          if (newRectangle.halfWidth < 0.1 || newRectangle.halfHeight < 0.1) {
            delete world[newRectangle.id];
            newRectangle = null;
          } else {
            // commit new rectangle to world
            newRectangle.born = true;
            box.addBody(newRectangle);
            addedRectangles.push(newRectangle);
            var mesaga = "{\"x\":" + newRectangle.x + ", \"y\":" + newRectangle.y +
                         ", \"width\":" + newRectangle.halfWidth + ", \"height\":" + newRectangle.halfHeight + " } ";
            console.log(mesaga);
            ws.send(mesaga);
            newRectangle = null;
          }
        }
      }

      box.update();
      bodiesState = box.getState();

      for (var id in bodiesState) {
        var entity = world[id];
        if (entity) entity.update(bodiesState[id]);
      }
    }

    var canvas = document.getElementById("c0");
    var ctx = canvas.getContext("2d");
    var canvasWidth = canvas.width;
    var canvasHeight = canvas.height;

    function draw() {
      ctx.clearRect(0, 0, canvasWidth, canvasHeight);
      for (var id in world) {
        var entity = world[id];
        entity.draw(ctx);
      }
    }

    var defaultInitialState = initialState = {
      "ground": {id: "ground", x: canvasWidth / 2 / SCALE, y: canvasHeight/ SCALE, halfHeight: 0.5, halfWidth: canvasWidth / SCALE, color: 'gray'}
    };

    var running = true;

    function init() {
      world = {};
      addedRectangles = [];
      removedRectangles = [];
      for (var i in initialState) {
        world[initialState[i].id] = Entity.build(initialState[i]);
      }

      box = new bTest(60, false, canvasWidth, canvasHeight, SCALE);
      box.setBodies(world);

      // setTimeout(function() {
      //   init();
      // }, 7000);
    }

    /* ---- INPUT ----------------------------- */
    var mouseX, mouseY, isMouseDown;

    canvas.addEventListener("mousedown", function(e) {
       isMouseDown = true;
       handleMouseMove(e);

       document.addEventListener("mousemove", handleMouseMove, true);
    }, true);

    document.addEventListener("mouseup", function() {
        if (!isMouseDown) return;
       document.removeEventListener("mousemove", handleMouseMove, true);
       isMouseDown = false;
       mouseX = undefined;
       mouseY = undefined;
    }, true);

    function handleMouseMove(e) {
       mouseX = (e.clientX - canvas.getBoundingClientRect().left) / SCALE;
       mouseY = (e.clientY - canvas.getBoundingClientRect().top) / SCALE;
    };

    /* ------------------------------------------ */

    document.addEventListener("DOMContentLoaded", function() {
      init();

      loadSavedWorlds();

      (function loop(animStart) {
        update(animStart);
        draw();
        requestAnimFrame(loop);
      })();
    }, false);

    document.getElementById("undo-rect").addEventListener("click", function(e) {
      if (addedRectangles.length == 0) return;
      var undid = addedRectangles.pop();
      removedRectangles.push(undid);
      delete world[undid.id];
      box.removeBody(undid.id);
      document.getElementById("redo-rect").disabled = false;
      if (addedRectangles.length == 0) e.target.disabled = true;
    });

    document.getElementById("redo-rect").addEventListener("click", function(e) {
      if (removedRectangles.length == 0) return;
      var redid = removedRectangles.pop();
      addedRectangles.push(redid);
      world[redid.id] = redid;
      box.addBody(redid);
      document.getElementById("undo-rect").disabled = false;
      if (removedRectangles.length == 0) e.target.disabled = true;
    });

    copyButton.addEventListener("click", function(e) {
      if (selectedId == null) return;

      var entity = world[selectedId];
      var copy = new RectangleEntity(Date.now(), entity.x, entity.y, entity.angle, entity.center, entity.color, entity.halfWidth, entity.halfHeight);
      world[copy.id] = copy;
      box.addBody(copy);
      addedRectangles.push(copy);
    });

    document.getElementById("save-world").addEventListener("click", function(e) {
      var select = document.getElementById("saved-worlds");
      var key = Date.now();
      localStorage[key] = JSON.stringify(world);
      select.appendChild(new Option(key));
    });

    document.getElementById("saved-worlds").addEventListener("click", function(e) {
      initialState = JSON.parse(localStorage[e.target.value]);
      init();
    });

    document.getElementById("delete-world").addEventListener("click", function(e) {
      var select = document.getElementById("saved-worlds");
      localStorage.removeItem(select.value);
      loadSavedWorlds();
      initialState = defaultInitialState;
      init();
    });

    function loadSavedWorlds() {
      var select = document.getElementById("saved-worlds");
      select.innerHTML = '';
      for (var k in localStorage) {
        select.appendChild(new Option(k));
      }
    }