package ru.corax.api

import javax.inject.Inject

import akka.actor.ActorRefFactory
import com.typesafe.scalalogging.slf4j.LazyLogging
import ru.corax.server.routers.APIComponent
import spray.httpx.ResponseTransformation
import spray.httpx.encoding.Gzip
import spray.routing.Directives
import spray.httpx.PlayTwirlSupport._


class SimpleAPI @Inject()()(implicit refFactory: ActorRefFactory)
  extends APIComponent
    with Directives
    with ResponseTransformation
    with LazyLogging {

  def route =
    path("partner" / "api" / "json") {
      post {
        entity(as[String]) { value =>
          complete("ok")
        }
      }
    } ~
      pathPrefix("assets") {
        compressResponse(Gzip) {
          getFromResourceDirectory("assets")
        }
      } ~
      pathPrefix("css") {
        compressResponse(Gzip) {
          getFromResourceDirectory("css")
        }
      } ~
      pathPrefix("js") {
        compressResponse(Gzip) {
          getFromResourceDirectory("js")
        }
      } ~
      path("simple") {
        get {
          complete(html.simple.render("hell"))
        }
      } ~
      path("app") {
        get {
          complete(html.app.render())
        }
      }
}
