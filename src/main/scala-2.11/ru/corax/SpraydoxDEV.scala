package ru.corax

import com.google.inject.Guice
import ru.corax.guice.MainModule
import ru.corax.server.SpraydoxServer

object SpraydoxDEV {
  def main(args: Array[String]) {
    Guice.createInjector(new MainModule).getInstance(classOf[SpraydoxServer]).init()
  }
}
