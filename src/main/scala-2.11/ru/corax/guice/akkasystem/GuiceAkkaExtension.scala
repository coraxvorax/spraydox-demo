package ru.corax.guice.akkasystem

import akka.actor._
import com.google.inject.Injector

import scala.reflect._

object GuiceAkkaExtension extends ExtensionId[GuiceAkkaExtension] with ExtensionIdProvider {
  override def lookup() = {
    GuiceAkkaExtension
  }

  override def createExtension(system: ExtendedActorSystem) = {
    new GuiceAkkaExtension
  }
}

class GuiceAkkaExtension extends Extension {
  private var injector: Injector = _

  def initialize(injector: Injector) {
    this.injector = injector
  }

  def props[A <: Actor : ClassTag] = {
    Props(classTag[GuiceActorProducer[A]].runtimeClass, injector, classTag[A].runtimeClass)
  }
}

class GuiceActorProducer[A <: Actor](injector: Injector, actorClz: Class[A])
  extends IndirectActorProducer {
  override def actorClass = {
    actorClz
  }

  override def produce = {
    injector.getInstance(actorClass)
  }
}


