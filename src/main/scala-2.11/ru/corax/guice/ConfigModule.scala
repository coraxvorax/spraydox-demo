package ru.corax.guice

import com.typesafe.config.ConfigValueType._
import com.typesafe.config.{Config, ConfigFactory, ConfigRenderOptions, ConfigValue}
import com.typesafe.scalalogging.slf4j.LazyLogging
import net.codingwell.scalaguice.BindingExtensions._
import net.codingwell.scalaguice.ScalaModule

import scala.collection.JavaConversions._

class ConfigModule extends ScalaModule with LazyLogging {
  def configure() {
    val config = loadConfig()
    bind[Config].toInstance(config)
    bindConfig(config)
  }

  protected[this] def loadConfig() = {
    ConfigFactoryExt.enableEnvOverride()
    val config = ConfigFactory.load
    logger.trace(s"${config.root.render(ConfigRenderOptions.concise.setFormatted(true))}")
    config
  }

  private def bindConfig(config: Config) {
    for (entry <- config.entrySet) {
      val cv = entry.getValue
      cv.valueType match {
        case STRING | NUMBER | BOOLEAN =>
          bindPrimitive(entry.getKey, entry.getValue)
        case LIST =>
          bindList(entry.getKey, entry.getValue)
        case NULL =>
          throw new AssertionError(
            s"Did not expect NULL entry in ConfigValue.entrySet: ${cv.origin}"
          )
        case OBJECT =>
          throw new AssertionError(
            s"Did not expect OBJECT entry in ConfigValue.entrySet: ${cv.origin}"
          )
      }
    }
  }

  private def bindPrimitive(key: String, value: ConfigValue) {
    val unwrapped = value.unwrapped.toString
    binderAccess.bindConstant.annotatedWithName(key).to(unwrapped)
  }

  private def bindList(key: String, value: ConfigValue) {
    val list = value.unwrapped.asInstanceOf[java.util.List[Any]]
    if (list.size == 0) {
      bind[Seq[Any]].annotatedWithName(key).toInstance(Seq())
      bind[Seq[String]].annotatedWithName(key).toInstance(Seq())
    } else {
      val seq = list.get(0) match {
        case x: Integer =>
          val v = list.collect({ case x: java.lang.Integer => x.intValue }).toSeq
          bind[Seq[Any]].annotatedWithName(key).toInstance(v)
        case x: Double =>
          val v = list.collect({ case x: java.lang.Double => x.doubleValue }).toSeq
          bind[Seq[Any]].annotatedWithName(key).toInstance(v)
        case x: Boolean =>
          val v = list.collect({ case x: java.lang.Boolean => x.booleanValue }).toSeq
          bind[Seq[Any]].annotatedWithName(key).toInstance(v)
        case x: String =>
          val v = list.collect({ case x: String => x }).toSeq
          bind[Seq[String]].annotatedWithName(key).toInstance(v)
        case x =>
          throw new AssertionError("Unsupported list type " + x.getClass)
      }
    }
  }
}

object ConfigFactoryExt {
  def enableEnvOverride() {
    val env = System.getProperty("env")
    if (env != null) {
      System.setProperty("config.resource", env + ".conf")
    }
  }
}