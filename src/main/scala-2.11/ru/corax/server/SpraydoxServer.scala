package ru.corax.server

import javax.inject.{Inject, Named}

import akka.actor.{ActorRef, ActorSystem}
import akka.io.IO
import com.typesafe.config.ConfigFactory
import ru.corax.guice.akkasystem.RouteActor
import spray.can.Http
import spray.can.server.{ServerSettings, UHttp}

class SpraydoxServer @Inject()(@Named("ApisRouter") defaultRouter: RouteActor)(implicit actorSystem: ActorSystem) {
  def init(): Unit = {
    IO(Http) ! Http.Bind(defaultRouter.actor, defaultRouter.host, port = defaultRouter.port,
      settings = Some(ServerSettings(actorSystem.settings.config.withValue("spray.can.server.ssl-encryption",
        ConfigFactory.parseString("ssl-encryption = off").getValue("ssl-encryption")))))
  }
}
