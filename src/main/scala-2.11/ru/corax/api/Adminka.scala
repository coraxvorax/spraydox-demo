package ru.corax.api

import javax.inject.Inject

import com.typesafe.scalalogging.slf4j.LazyLogging
import ru.corax.server.routers.APIComponent
import spray.routing.Directives
import spray.httpx.PlayTwirlSupport._

class Adminka @Inject()
  extends APIComponent
    with Directives
    with LazyLogging {

  def route =
    path("adminka" / "home") {
      get {
        parameter('msg) { (message) =>
          complete {
            html.index.render(message)
          }
        }
      } ~
        post {
          entity(as[String]) { value =>
            complete("ok")
          }
        }
    }
}
