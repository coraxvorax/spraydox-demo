package ru.corax.server.routers

import javax.inject.Inject

import akka.actor.Actor
import akka.event.Logging
import com.typesafe.scalalogging.slf4j.LazyLogging
import spray.http.StatusCodes
import spray.routing._
import spray.util.LoggingContext

class DefaultRouteActor @Inject()(apiSet: Set[APIComponent]) extends Actor with SpraydoxService {
  def actorRefFactory = {
    context
  }

  def receive = {
    runRoute(defaultRoute)
  }

  protected[this] def apis = apiSet.toSeq
}

trait APIComponent {
  def route: Route
}

trait SpraydoxService extends HttpService with LazyLogging {
  protected[this] def apis: Seq[APIComponent]

  lazy val defaultRoute =
    logRequestResponse("MARK", Logging.InfoLevel) {
      handleRejections(rejectionHandler) {
        handleExceptions(exceptionHandler) {
          apis.tail.foldLeft(apis.head.route) { (chain, next) =>
            chain ~ next.route
          }
        }
      }
    }

  override def timeoutRoute: Route = {
    complete(
      StatusCodes.ServiceUnavailable,
      "The server could not provide a timely response."
    )
  }

  private def exceptionHandler(implicit log: LoggingContext) = {
    ExceptionHandler {
      case e: ExceptionWithStatus =>
        complete(e.statusCode, e.msg)
    }
  }

  private val rejectionHandler = {
    RejectionHandler {
      case MissingQueryParamRejection(param) :: _ =>
        complete(StatusCodes.BadRequest, s"Request is missing required query parameter '$param'")
    }
  }
}

case class ExceptionWithStatus(statusCode: Int, msg: String, cause: Throwable = null) extends Exception(msg, cause)
