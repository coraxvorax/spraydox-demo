

name := "spraydox-demo"

version := "1.0"

scalaVersion := "2.11.8"

lazy val root = Project("template-spraydox", file(".")).enablePlugins(SbtTwirl)

mainClass in assembly := Some("ru.corax.Spraydox")

assemblyJarName in assembly := "spraydox.jar"

seq(coffeeSettings: _*)

seq(lessSettings:_*)

{
  val AkkaVersion = "2.3.7"
  val SprayVersion = "1.3.2"
  val Json4sVersion = "3.2.11"
  val Log4jVersion = "2.1"
  libraryDependencies ++= Seq(
    "io.spray" %% "spray-can" % SprayVersion,
    "io.spray" %% "spray-client" % SprayVersion,
    "com.wandoulabs.akka" %%  "spray-websocket"       % "0.1.4",
    "io.spray" %% "spray-json" % "1.3.1",
    "io.spray" %% "spray-routing" % SprayVersion,
    "org.apache.logging.log4j" % "log4j-api" % Log4jVersion,
    "org.apache.logging.log4j" % "log4j-core" % Log4jVersion,
    "org.apache.logging.log4j" % "log4j-slf4j-impl" % Log4jVersion,
    "com.typesafe.akka" %% "akka-actor" % AkkaVersion,
    "com.typesafe.akka" %% "akka-slf4j" % AkkaVersion,
    "com.typesafe" % "config" % "1.2.1",
    "com.typesafe.scala-logging" %% "scala-logging-slf4j" % "2.1.2",
    "tanukisoft" % "wrapper" % "3.2.3",
    "net.codingwell" %% "scala-guice" % "4.0.0-beta4",
    "org.json4s" %% "json4s-jackson" % Json4sVersion,
    "org.json4s" %% "json4s-ext" % Json4sVersion,
    "com.googlecode.flyway" % "flyway-core" % "2.3.1",
    "org.apache.httpcomponents" % "fluent-hc" % "4.5.2"
  )
}
    