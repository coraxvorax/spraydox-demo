package ru.corax

import com.google.inject.Guice
import org.tanukisoftware.wrapper.{WrapperListener, WrapperManager}
import ru.corax.guice.MainModule
import ru.corax.server.SpraydoxServer

class Spraydox extends WrapperListener {

  val injector = Guice.createInjector(new MainModule)

  override def controlEvent(i: Int): Unit = println("controled")

  override def stop(i: Int): Int = 0

  override def start(strings: Array[String]): Integer = {
    injector.getInstance(classOf[SpraydoxServer]).init()
    null
  }

}

object Spraydox {
  def main(args: Array[String]) {
    WrapperManager.start(new Spraydox, args)
  }
}
