package ru.corax.guice.api

import net.codingwell.scalaguice.{ScalaModule, ScalaMultibinder}
import ru.corax.api.{Adminka, SimpleAPI}
import ru.corax.server.routers.APIComponent

class ApisModule extends ScalaModule {
  private lazy val apiBinder = ScalaMultibinder.newSetBinder[APIComponent](binder)

  protected[this] def bindApi = {
    apiBinder.addBinding
  }

  def configure() {
    bindApi.to[SimpleAPI]
    bindApi.to[Adminka]
  }
}
