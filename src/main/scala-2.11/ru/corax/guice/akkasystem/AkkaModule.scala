package ru.corax.guice.akkasystem

import javax.inject.{Provider, Singleton}

import akka.actor.{ActorRefFactory, ActorSystem}
import com.google.inject.{Injector, Provides}
import net.codingwell.scalaguice.ScalaModule

import scala.concurrent.ExecutionContext

class AkkaModule extends ScalaModule {
  def configure() {
  }

  @Provides
  @Singleton
  def provideActorSystem(injector: Injector): ActorSystem = {
    val system = ActorSystem("root-actor-system")
    GuiceAkkaExtension(system).initialize(injector)
    system
  }

  @Provides
  @Singleton
  def provideActorRefFactory(systemProvider: Provider[ActorSystem]): ActorRefFactory = {
    systemProvider.get
  }

  @Provides
  @Singleton
  def provideExecutionContext(systemProvider: Provider[ActorSystem]): ExecutionContext = {
    systemProvider.get.dispatcher
  }
}
