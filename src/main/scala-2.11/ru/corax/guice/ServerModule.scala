package ru.corax.guice

import javax.inject.{Named, Singleton}

import akka.actor.{ActorRef, ActorSystem}
import com.google.inject.Provides
import net.codingwell.scalaguice.ScalaModule
import ru.corax.guice.akkasystem.{GuiceAkkaExtension, RouteActor}
import ru.corax.server.SpraydoxServer
import ru.corax.server.routers.DefaultRouteActor

class ServerModule extends ScalaModule {
  override def configure(): Unit = {
    bind[SpraydoxServer]
  }

  @Provides
  @Singleton
  @Named("ApisRouter")
  def provideApiRouterActorRef(system: ActorSystem): RouteActor = {
    RouteActor(system.actorOf(GuiceAkkaExtension(system).props[DefaultRouteActor]), "localhost", 8888)
  }
}
