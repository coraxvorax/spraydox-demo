package ru.corax.guice

import net.codingwell.scalaguice.ScalaModule
import ru.corax.guice.akkasystem.AkkaModule
import ru.corax.guice.api.ApisModule

class MainModule extends ScalaModule {
  override def configure() {
    installCore()
    installServer()
  }

  private def installCore() {
    install(new ConfigModule)
    install(new AkkaModule)
  }

  private def installServer() {
    install(new ServerModule)
    install(new ApisModule)
  }
}
